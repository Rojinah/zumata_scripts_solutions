// @ts-check

import http from 'http';

import polka from 'polka';
import send from '@polka/send-type';
import cors from 'cors';
import fetch from 'node-fetch';
import bodyParser from 'body-parser';
import html from './html.js';
import users from './users.js';
import helper from './helper';
import {join} from 'path';
import {renderFile} from 'ejs';
const PORT = 5000;
const app = polka();
const views = join(__dirname, './form.html');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/healthcheck', (_, res) => res.end('OK'));
app.post('/mocky-api', function (req, res) {
  // TODO: Refactor to use ES2015+, fat arrow function, and async...await
  var url = 'http://www.mocky.io/v2/5b60920b2f00008e364619ee?mocky-delay=' + Math.floor(Math.random() * 3e3) + 'ms';
  
  // return fetch(url, {
  //   timeout: 10e3,
  //   agent: new http.Agent({ keepAlive: true }),
  // })
  //   .then(function (r) {
  //     if (!r.ok || r.status > 399) {
  //       throw new Error('Fetch error');
  //     }
    
  //     return r.json();
  //   })
  //   .then(function (d) {
  //     return send(res, 200, d);
  //   })
  //   .catch(function (e) {
  //     console.error('Unable to fetch', e);
    
  //     return send(
  //       res,
  //       500,
  //       { error: { message: e.message } },
  //       { 'Content-Type': 'application/json' });
  //   });

  async function searchData(url) {
    const response = await fetch(url);
    if (response.ok) {
        const json = await response.json();
        return send(res,200,json,{'Content-Type':'application/json'});
    }
    throw new Error("Could not retrieve results from url");
  }
    
  async function getUrlData() {
    try {
        var url = 'http://www.mocky.io/v2/5b60920b2f00008e364619ee?mocky-delay=' + Math.floor(Math.random() * 3e3) + 'ms';
        const albums = await searchData(url);
        
    } catch (err) {
        console.error(`Failed to retrieve results: ${err.message}`);
        throw err;
    }
  }
  getUrlData().catch(err => console.err(err.message));
});
app.post('/data', (req, res) => {
  // TODO: Refactor so that uid can be string or an array of strings
  const uid  = req.query;
  var array =[];
  array.push(uid['uid']);
  var array = JSON.parse('[' + array + ']');
  var promises = [];
  array.forEach((id) => {
    promises.push(helper.make_API_call(id))
  })

  Promise.all(promises)
  .then((result) => {
    console.log('all resolved ', result)
    return send(res, 200, result,{'Content-Type':'application/json'});
  })
});

app.get('/', async (_, res) => {
  let msg =users.users.find(n => n.id === 99);
  const d = await html`<h1>${JSON.stringify(msg)}</h1>`;
  
  return send(res, 200, d, { 'Content-Type': 'text/html' });
});

app.get('/form', function(req, res) {
  // const data = {qs:req.params};
    renderFile(views, (err, html) => {
        if (err) return send(res, 500,err.message || err);
      return send(res, 200, html,{'Content-Type':'text/html'});
    });
  // };
});

app.post('/form-submission', function(req,res){
  const successMsg = "Form Submitted Successfully";
  req.cdata = {
                success: 1,
                reqData:req.body,
                message: 'Form Submitted Successfully'
            };
  return send(res, 200, req.cdata,successMsg, { 'Content-Type': 'application/x-www-form-urlencoded','Content-Length': 99});
});

app
  .listen(PORT)
  .then(() => {
    console.info(`Polka running at port ${PORT}...`)
});